package exo1_5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import exo1_3.CesarWriter;

public class MainClass {
	public static void main(String[] args)
	{
		String str = "";
		
		System.out.println("Debut");
		
		try {
			BufferedReader r = new BufferedReader(new FileReader(args[0]));
			Writer w = new CesarWriter(new BufferedWriter(new FileWriter("output.txt")));
			while ( (str = r.readLine()) != null)
			{
				w.write(str, 0, str.length());
				System.out.println(str);
			}
			r.close();
			w.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("Fin");
	}
}