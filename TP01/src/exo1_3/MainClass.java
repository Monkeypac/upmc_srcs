package exo1_3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;

public class MainClass {
	public static void main(String[] args)
	{
		String str = "";
		
		try {
			BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
			Writer w = new CesarWriter(new BufferedWriter(new FileWriter("output.txt")));
			while ( (str = r.readLine()) != null)
			{
				w.write(str, 0, str.length());
				System.out.println(str);
			}
			r.close();
			w.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("Fin");
	}
}
