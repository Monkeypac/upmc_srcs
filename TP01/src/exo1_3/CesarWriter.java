package exo1_3;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

public class CesarWriter extends FilterWriter{

	public static int decalage = 5;
	
	public CesarWriter(Writer out) {
		super(out);
	}
	
	public void write(String s, int a, int b)
	{
		for(int i = a; i < b; i++)
		{
			char c = s.charAt(i);
			int toWrite = 'a' + (c - 'a' + decalage)%26;
			try {
				super.write( toWrite );
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void write(int i)
	{
		if(i >= 'a' && i <= 'z')
		{
			int toWrite = 'a' + ((char)i - 'a' + decalage)%26;
			try {
				super.write(toWrite);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				super.write(i);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
