/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exo2_1;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author monkeypac
 */
public class CesarStream extends FilterOutputStream{
    public static int decalage = 5;
    
    public CesarStream(OutputStream out) {
        super(out);
    }
    
    public void write(String s, int a, int b)
    {
        for(int i = a; i < b; i++)
        {
            char c = s.charAt(i);
            try {
                super.write(convert(c));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void write(int i)
    {
        try
        {
            super.write(convert(i));
        } catch (IOException ex) {
            Logger.getLogger(CesarStream.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static int convert(int i)
    {
        if(i >= 'a' && i <= 'z')
        {
            int toWrite = 'a' + ((char)i - 'a' + decalage)%26;
            return toWrite;
        }
        else
        {
            return i;
        }
    }
    
    public static int unconvert(int i)
    {
        if(i >= 'a' && i <= 'z')
        {
            int toWrite = 'a' + ((char)i - 'a' - decalage)%26;
            return toWrite;
        }
        else
        {
            return i;
        }
    }
}
