package exo2_1;

import exo1_3.CesarWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Client {
	private static String str = "[CLIENT] ";
	private static InetSocketAddress localhost = new InetSocketAddress("localhost", 8080);
	private static int try_times = 5;


	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Socket sock = null;
		OutputStream os = null;
		sock = new Socket();
		int i = 0;

		while(i < try_times && !sock.isConnected())
		{
			try {
				sock = new Socket();
				sock.connect(localhost, 8080);
			}
			catch (IllegalArgumentException e){
				e.printStackTrace();
			}
			catch (IOException e) {
				i++;
				System.out.println("Couldnt connect ... Trying again ...");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		if(!sock.isConnected())
		{
			System.out.println("Couldnt connect to the server!");
			return;
		}
		
		System.out.println(str
				+ "Port server : " + sock.getPort() + "\n" + str
				+ "Port client : " + sock.getLocalPort());
		
		
		int read_value;
		try {
			os = new CesarStream(sock.getOutputStream());
			InputStream r = System.in;
			while ( (read_value = r.read()) != -1 && read_value != 4)
			{
				os.write((byte)read_value);
			}
			r.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try
		{
			sock.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
