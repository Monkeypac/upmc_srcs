package exo2_1;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	private static String str = "[SERVER] "; 
	private static InetSocketAddress localhost = new InetSocketAddress("localhost", 8080);
	private static int nb_clients = 1;
	
	public static void main(String[] args)
	{
		ServerSocket server = null;
		List<Socket> sock = new ArrayList<>();
		InputStream is = null;
		
		try {
			server = new ServerSocket(8080);
			
			for (int i = 0; i < nb_clients; i++)
				sock.add(server.accept());
			
			for (Socket s : sock)
				System.out.println(str
						+ "Port server : " + server.getLocalPort() + "\n" + str
						+ "Port client : " + s.getPort());
			
			is = sock.get(0).getInputStream();
			int read_value;
			try {
                                String s = "";
				while ( (read_value = is.read()) >= 0 && read_value != 4)
				{
					System.out.print((char)read_value);
                                        s += (char)CesarStream.unconvert(read_value);
                                        if (read_value == '\n') {
                                            System.out.print(s);
                                            s = "";
                                        }
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
					
			server.close();
			
			for (Socket s : sock)
				s.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
