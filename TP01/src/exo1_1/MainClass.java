package exo1_1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class MainClass {
	public static void main(String[] args)
	{
		String str = "";
		
		try {
			BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
			while ( (str = r.readLine()) != null)
			{
				System.out.println(str);
			}
			r.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("Fin");
	}
}
