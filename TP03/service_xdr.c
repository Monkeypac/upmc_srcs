/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "service.h"

bool_t
xdr_my_data (XDR *xdrs, my_data *objp)
{
	register int32_t *buf;

	 if (!xdr_string (xdrs, &objp->a, ~0))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->b, ~0))
		 return FALSE;
	return TRUE;
}
