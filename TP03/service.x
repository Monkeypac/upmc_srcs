struct my_data {
	string a<>;
	string b<>;
};

program myprog {
	version TP {
		int DOUBLE (int) = 1;
		void PRINT (struct my_data) = 2;
		string CONCAT (struct my_data) = 3;
		void INC () = 4;
	} = 1;
} = 1;
