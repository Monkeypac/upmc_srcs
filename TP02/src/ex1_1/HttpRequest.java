package ex1_1;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;

import com.sun.xml.internal.ws.util.StringUtils;

import upmc.InputStreamLiner;

public class HttpRequest extends Thread{
	Socket sock;
	InputStreamLiner isl;
	DataOutputStream dos;
	String head;
	String line;
	File file;
	int length;
	
	static final int FILE_OS = InputStreamLiner.UNIX;
	
	public HttpRequest(Socket s)
	{
		super();
		sock = s;
		try
		{
			isl = new InputStreamLiner(sock.getInputStream());
			dos = new DataOutputStream(sock.getOutputStream());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void processRequest()
	{
		System.out.println("Process Request");
		try {
			int requestType;
			head = isl.readLine(FILE_OS);
			System.out.println(head);
			if( (requestType = checkRequest(head)) != 0)
			{
				while( (line=isl.readLine(FILE_OS)) != null && !line.equals("") && !line.equals("\r"))
				{
					String tmp[] = line.split(":");
					if(tmp[0].equals("Content-Length"))
						length = Integer.parseInt(tmp[1]);
					System.out.println(">" + line);
				}
				
				System.out.println(requestType);
				switch(requestType)
				{
				case 1:
					processGet();
					break;
				case 2:
					processPut();
					break;
				default:
					System.out.println("I dont know this!");
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void processGet()
	{
		System.out.println("Process Get");
		try {
			String str;
			BufferedReader fr = new BufferedReader(new FileReader(file));
			System.out.println("Writing ...");
			
			String rep_ok = "HTTP/1.1 200 OK\n\r\n\r";
			dos.writeBytes(rep_ok);
			while( (str = fr.readLine()) != null)
				dos.writeBytes(str + "\n");
			dos.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public void processPut()
	{
		System.out.println("Process Put");
		int c;
		try {
			BufferedWriter wr = new BufferedWriter(new FileWriter(file));
			while(length > 0 && (c=isl.read()) != -1) {
				System.out.print((char)c);
				wr.write((char)c);
				length--;
				System.out.println("\t" + length);
			}
			
			String rep_ok = "HTTP/1.1 200 OK\n\r\n\r";
			dos.writeBytes(rep_ok);
			dos.close();
			wr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Put over");
	}
	
	public void run()
	{
		processRequest();
	}
	
	public int checkRequest(String head)
	{
		String tmp[] = head.split(" ");
		
		file = new File(tmp[1]);
		System.out.println(file.getName());
		if(!tmp[2].substring(0, 4).equals("HTTP"))
				return 0;
		
		if(tmp[0].equals("GET")){
			if(!file.exists())
				return 0;
			return 1;
		}
		else if(tmp[0].equals("PUT"))
			return 2;
		return 0;
	}
}
