/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1_1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author monkeypac
 */
public class Server {
	private static String str = "[SERVER] "; 
	
	public static void main(String[] args)
	{
		ServerSocket server = null;
		List<Socket> list = new ArrayList<>();
		List<HttpRequest> hlist = new ArrayList<>();
		Socket sock;
		
		try {
			server = new ServerSocket(8080);
			
			while(true)
			{
            
				sock = server.accept();
				
				list.add(sock);
				
	            hlist.add(new HttpRequest(sock));
	            
	            hlist.get(hlist.size()-1).start();
	            
	            System.out.println(str
	                            + "Port server : " + server.getLocalPort() + "\n" + str
	                            + "Port client : " + sock.getPort());
			}
			
			/*for(Socket s : list)
				s.close();
					
			server.close();
			*/
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
