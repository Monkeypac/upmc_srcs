package ex1_2;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import upmc.InputStreamLiner;

public class Client {
	private static String str = "[CLIENT] ";
	private static InetSocketAddress localhost = new InetSocketAddress("localhost", 8080);
	private static int try_times = 5;


	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		testGET();
		testPUT();
	}
	
	public static void testGET()
	{
		Socket sock = null;
		DataOutputStream os = null;
		InputStreamLiner isl = null;
		sock = new Socket();
		int i = 0;

		while(i < try_times && !sock.isConnected())
		{
			try {
				sock = new Socket();
				sock.connect(localhost, 8080);
			}
			catch (IllegalArgumentException e){
				e.printStackTrace();
			}
			catch (IOException e) {
				i++;
				System.out.println("Couldnt connect ... Trying again ...");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		if(!sock.isConnected())
		{
			System.out.println("Couldnt connect to the server!");
			return;
		}
		
		System.out.println(str
				+ "Port server : " + sock.getPort() + "\n" + str
				+ "Port client : " + sock.getLocalPort());
		
		try {
			os = new DataOutputStream(sock.getOutputStream());
			
			String str = "GET file.txt HTTP/0\n"
					+ "entete1:valeur1\n"
					+ "entete2:valeur2\n\n";
			
			os.writeBytes(str);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try
		{
			String str;
			isl = new InputStreamLiner(sock.getInputStream());
			
			while( (str = isl.readLine(InputStreamLiner.UNIX)) != null)
				System.out.println(str);
			
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			sock.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void testPUT()
	{
		Socket sock = null;
		DataOutputStream os = null;
		InputStreamLiner isl = null;
		sock = new Socket();
		int i = 0;

		while(i < try_times && !sock.isConnected())
		{
			try {
				sock = new Socket();
				sock.connect(localhost, 8080);
			}
			catch (IllegalArgumentException e){
				e.printStackTrace();
			}
			catch (IOException e) {
				i++;
				System.out.println("Couldnt connect ... Trying again ...");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		if(!sock.isConnected())
		{
			System.out.println("Couldnt connect to the server!");
			return;
		}
		
		System.out.println(str
				+ "Port server : " + sock.getPort() + "\n" + str
				+ "Port client : " + sock.getLocalPort());
		
		try {
			os = new DataOutputStream(sock.getOutputStream());
			
			String filestr = "Bonjour je vais etre ecrite!\n\nEt jai une ligne vide!";
			
			String str = "PUT fileput.txt HTTP/0\n"
					+ "Content-Length:" + filestr.length() + "\n"
					+ "entete2:valeur2\n"
					+ "\n"
					+ filestr;
			
			System.out.println(str);
			
			os.writeBytes(str);
			System.out.println("Sent!");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try
		{
			String str;
			isl = new InputStreamLiner(sock.getInputStream());
			
			while( (str = isl.readLine(InputStreamLiner.UNIX)) != null)
				System.out.println(str);
			
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			sock.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
